

# Imports Available in Qt5.11.1
# fetch with:
# cd 
# grep -r "import QtQuick" * | gawk -F' ' '{print $2,$3}' | sort -u

# QtQml 2.2
# QtQuick 2.11
# QtQuick.Controls 2.4
# QtQuick.Controls.Fusion 2.4

# QtQuick.Controls.Imagine 2.4
# QtQuick.Controls.Material 2.4
# QtQuick.Controls.Private 1.0
# QtQuick.Controls.Styles 1.4
# QtQuick.Controls.Universal 2.4
# QtQuick.Dialogs 1.2
# QtQuick.Dialogs.Private 1.1
# QtQuick.Extras 1.4
# QtQuick.Extras.Private 1.1
# QtQuick.Extras.Private.CppUtils 1.1
# QtQuick.Layouts 1.3
# QtQuick.PrivateWidgets 1.1
# QtQuick.Templates 2.4
# QtQuick.tooling 1.2
# QtQuick.Window 2.3


grep -l -r "import QtQuick.Controls 2." *.qml | xargs sed -i 's/QtQuick.Controls 2\.[0-9]/QtQuick.Controls 2.4/'

grep -l "import QtQuick.Controls.Material" *.qml | xargs sed -i 's/QtQuick.Controls.Material 2\.[0-9]/QtQuick.Controls.Material 2.4/'




