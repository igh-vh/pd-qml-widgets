/*****************************************************************************
 *
 * Copyright (C) 2018-2022  Bjarne von Horn <vh@igh.de>,
 *                          Wilhelm Hagemeister<hm@igh.de>,
 *                          Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QML_PD_WIDGETS2_H
#define QML_PD_WIDGETS2_H

#include <QtQml>
#include <QQmlEngine>
#include "ScalarVariant.h"
#include "VectorVariant.h"

namespace Pd {

inline void setQmlImportPathToResourceFile(QQmlEngine &engine)
{
    engine.addImportPath("qrc:///");
}

inline int registerScalarVariant()
{
    return qmlRegisterType<Pd::ScalarVariant>("de.igh.pd", 2, 0, "PdScalar");
}

inline int registerVectorVariant()
{
    return qmlRegisterType<Pd::VectorVariant>("de.igh.pd", 2, 0, "PdVector");
}

}
#endif //  QML_PD_WIDGETS2_H
