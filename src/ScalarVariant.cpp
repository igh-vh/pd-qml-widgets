/*****************************************************************************
 *
 * Copyright (C) 2018  Wilhelm Hagemeister <hm@igh.de>, with code from
 *                     Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include <PdQmlWidgets2/ScalarVariant.h>
using Pd::ScalarVariant;

#include <QtPdWidgets2/Transmission.h>

#include <QStringList>

//#define PD_DEBUG_SCALARVARIANT

/****************************************************************************/

/** Constructor.
 */
ScalarVariant::ScalarVariant():
    process(0),
    value(0),
    path(""),
    period(0),
    _scale(1.0),
    _offset(0.0),
    dataPresent(false)
{
}

/****************************************************************************/

/** Destructor.
 */
ScalarVariant::~ScalarVariant()
{
}

/****************************************************************************/

Pd::Transmission getTrans(double sampleTime)
{
    if (sampleTime > 0.0) {
        return { std::chrono::duration<double>(sampleTime) };
    }
    if (sampleTime == 0.0) {
        return { Pd::event_mode };
    }
    return { Pd::poll_mode, -sampleTime };
}

/****************************************************************************/

void ScalarVariant::updateConnection()
{
    // FIXME test for scalar here
    if (process and process->isConnected()) {
        // we first allow only scalar elements as selector
        QStringList pathElements = path.split('#',
#if QT_VERSION > 0x050f00
                Qt::SkipEmptyParts
#else
                QString::SkipEmptyParts
#endif
                );

        if (pathElements.count() > 1) {
            bool ok;
            int index = pathElements.at(1).toInt(&ok);
            if (!ok) {
                qCritical() << "Only integer as path selector allowed "
                    "currently! Not registering the variable " << path;
                return;
            }
            PdCom::ScalarSelector selector({index});
#ifdef PD_DEBUG_SCALARVARIANT
            qDebug() << "setvariable path: " << pathElements.at(0)
                << ", index " << index
                << ", period: " << period;
#endif

            setVariable(process, pathElements.at(0), selector,
                    getTrans(period), _scale, _offset);
        } else {
            //PdCom::Selector all;
#ifdef PD_DEBUG_SCALARVARIANT
            qDebug() << "setvariable path " << path
                << ", period " << period;
	    
#endif
            setVariable(process, path, {}, getTrans(period), _scale, _offset);
        }
    }
}

/****************************************************************************/

void ScalarVariant::setProcess(Pd::Process *value){

  //  qDebug() << "set Process" << value;
  if(value != process) {
    if(process) { //detach from "old" process
      clearVariable();
      QObject::disconnect(process,0,0,0);
    }
    if(value) {
      process = value;
      QObject::connect(process, SIGNAL(processConnected()), this, SLOT(processConnected()));
      QObject::connect(process, SIGNAL(disconnected()), this, SLOT(processDisconnected()));
      QObject::connect(process, SIGNAL(error()), this, SLOT(processError()));
      //updateConnection();
    }
    //emit processChanged();
  }
}

/****************************************************************************/

void ScalarVariant::processConnected()
{
  updateConnection();
}

/****************************************************************************/

void ScalarVariant::processDisconnected()
{
  clearData();
}

/****************************************************************************/

void ScalarVariant::processError()
{
  clearData();
}

/****************************************************************************/

QVariant ScalarVariant::getConnection(){
  QVariantMap e;

  e["path"] = path;
  e["period"] = period;
  e["offset"] = _offset;
  e["scale"] = _scale;
  return QVariant::fromValue(e);
}

/****************************************************************************/

void ScalarVariant::setConnection(QVariant value){

  if (value.canConvert<QVariantMap>()) {
    QVariantMap connection = value.toMap();

    if(connection.contains("process")) {
      Pd::Process *process = connection["process"].value<Pd::Process *>();
      setProcess(process);
    } else { //try default process
      setProcess(Pd::Process::getDefaultProcess());
    }

    connection.contains("path")?
      path = connection["path"].toString():
      path = "";

    period = 0;
    //"sampleTime"? backward compatibility
    connection.contains("sampleTime")?
      period = connection["sampleTime"].toDouble():period;

    connection.contains("period")?
      period = connection["period"].toDouble():period;

    connection.contains("offset")?
      _offset = connection["offset"].toDouble():
      _offset = 0;

    connection.contains("scale")?
      _scale = connection["scale"].toDouble():
      _scale = 1.0;

    updateConnection();
    emit connectionUpdated();
  } else {
    qCritical() << "connection has to be a map";
  }
}

/****************************************************************************/

void ScalarVariant::setPath(QString value){
   if(value != path) {
    path = value;
    period = 0;
    _offset = 0;
    _scale = 1;
    //the design choice is, that setting only the path is for
    //event transmitted variables from the defaultProcess
    setProcess(Pd::Process::getDefaultProcess());
    updateConnection();
    emit pathChanged(path);
  }
}

/****************************************************************************/

void ScalarVariant::clearData()
{
    value = 0;
    dataPresent = false;
    emit dataPresentChanged(dataPresent);
    emit valueChanged(value);
}

/****************************************************************************/

void ScalarVariant::setValue(QVariant value)
{
    if (not dataPresent or getVariable().empty()) {
        return;
    }

    //nothing to write
    if (value == ScalarVariant::value) {
      return;
    }

    switch (getVariable().getTypeInfo().type) {
        case PdCom::TypeInfo::boolean_T:
        case PdCom::TypeInfo::uint8_T:
        case PdCom::TypeInfo::uint16_T:
        case PdCom::TypeInfo::uint32_T:
        case PdCom::TypeInfo::uint64_T:
            if (value.canConvert<uint64_t>()) {
                writeValue((uint64_t) value.toULongLong());
            } else {
            qWarning() << "Variant datatype can't be converted to ULongLong";
            }
            break;
        case PdCom::TypeInfo::int8_T:
        case PdCom::TypeInfo::int16_T:
        case PdCom::TypeInfo::int32_T:
        case PdCom::TypeInfo::int64_T:
            if (value.canConvert<int64_t>()) {
                writeValue((int64_t) value.toLongLong());
            } else {
                qWarning() << "Variant datatype can't be converted to LongLong";
            }
            break;
        case PdCom::TypeInfo::single_T:
        case PdCom::TypeInfo::double_T:
            if(value.canConvert<double>()) {
                writeValue(value.toDouble());
            } else {
                qWarning() << "Variant datatype can't be converted to double";
            }
            break;
        default:
            qWarning() << "unknown datatype: can't write anything to process.";
            break;
    }
}

/****************************************************************************/

/** Increments the current #value and writes it to the process.
 *
 * This does \a not update #value directly.
 */
void ScalarVariant::inc()
{
  writeValue(value.toInt() + 1);
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void ScalarVariant::newValues(std::chrono::nanoseconds ts)
{
  QVariant v;

  uint8_t d;
  copyData(d);

#ifdef PD_DEBUG_SCALARVARIANT
  qDebug() << "type " << getVariable().getTypeInfo().type << "value"  << d;
#endif

  switch (getVariable().getTypeInfo().type) {
  case PdCom::TypeInfo::boolean_T:
  case PdCom::TypeInfo::uint8_T:
  case PdCom::TypeInfo::uint16_T:
  case PdCom::TypeInfo::uint32_T:
  case PdCom::TypeInfo::uint64_T:
    {
      uint64_t uInt;
      copyData(uInt);
      v = (quint64)(uInt * _scale + _offset);
    }
    break;
  case PdCom::TypeInfo::int8_T:
  case PdCom::TypeInfo::int16_T:
  case PdCom::TypeInfo::int32_T:
  case PdCom::TypeInfo::int64_T:
    {
      int64_t sInt;
      copyData(sInt);
      //qDebug() << "sint " << sInt;
      v = (qint64)(sInt * _scale + _offset);
    }
    break;
  case PdCom::TypeInfo::single_T:
  case PdCom::TypeInfo::double_T:
    {
      double d;
      copyData(d);
      v = (double)(d * _scale + _offset);
    }
    break;
  default:
    qWarning() << "unknown datatype";
    v = (double)0.0;
    break;
  }

#ifdef PD_DEBUG_SCALARVARIANT
    QString path = getVariable().getPath().data();
    qDebug() << "new value " << v << " for " << path;
#endif

    if(!dataPresent) {
      dataPresent = true;
#ifdef PD_DEBUG_SCALARVARIANT
      qDebug() << "emit data present: " << dataPresent;
#endif
      emit dataPresentChanged(dataPresent);
      value = v;
      emit valueChanged(value);
    } else { //dataPresent == true
      if(value != v) {
          value = v;
          emit valueChanged(value);
      }
    }

    mTime = ts;
#ifdef PD_DEBUG_SCALARVARIANT
    qDebug() << "emit value updated: "
        << std::chrono::duration<double>(mTime).count();
#endif

    emit valueUpdated(std::chrono::duration<double>(mTime).count());
}

/****************************************************************************/
