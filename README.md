------------------------------------------------------------------------------
vim: tw=78 syntax=markdown
------------------------------------------------------------------------------

# General Information

This is the Pd QML Widgets library, a process-data aware widget library for
Qt/QML from the EtherLab project. See
https://gitlab.com/etherlab.org/pd-qml-widgets

------------------------------------------------------------------------------

# License

The Pd QML Widgets library is licensed unter the terms and conditions of the
GNU Lesser General Public License (LGPL), version 3 or (at your option) any
later version.

------------------------------------------------------------------------------

# Prerequisites

   - Qt5
   - PdCom 5 (see http://etherlab.org/en/pdcom)
   - PdWidgets 2 (see http://etherlab.org/en/qtpdwidgets)

------------------------------------------------------------------------------

# Building and Installing

The build process is handled by qmake. To build and install, call:

qmake
make
make install

Have fun!

------------------------------------------------------------------------------
