#-----------------------------------------------------------------------------
#
# vim: syntax=config
#
# Copyright (C) 2022  Florian Pose <fp@igh.de>
#
# This file is part of the Pd QML Widgets library.
#
# The Pd QML Widgets library is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# The Pd QML Widgets library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the Pd QML Widgets Library. If not, see
# <http://www.gnu.org/licenses/>.
#
#-----------------------------------------------------------------------------

TEMPLATE = lib
TARGET = PdQmlWidgets

# good to know: if compiling for android the unix target is selected as well

# On release:
# - Change version in Doxyfile
# - Change version in .spec file
# - Add a NEWS entry
VERSION = 2.0.0

MAJOR = $$section(VERSION, ., 0, 0)
TARGET_MAJOR = $${TARGET}$${MAJOR} # with major number
BASENAME = $$TARGET # without major number

unix {
    TARGET = $${TARGET_MAJOR}
}

CONFIG += debug_and_release
QT += widgets network xml svg quick

# Path definitions (can be overridden via qmake command-line) ----------------

# Installation prefix (used for header installation only)
isEmpty(PD_INSTALL_PREFIX) {
    PD_INSTALL_PREFIX = $$[QT_INSTALL_PREFIX]
}

# Library installation path
isEmpty(PD_INSTALL_LIBS) {
    PD_INSTALL_LIBS = $$[QT_INSTALL_LIBS]
}

# DLL installation path
isEmpty(PD_INSTALL_BINS) {
    PD_INSTALL_BINS = $$[QT_INSTALL_BINS]
}

# QML installation path

# the evaluation of isEmpty() does not work properly
# on qmake properties, so use a helper variable
HELPER = $$[QT_INSTALL_QML]

isEmpty(PD_INSTALL_QML) {
    isEmpty(HELPER) {
        # fallback for older Qt5 versions
        PD_INSTALL_QML = $$[QT_INSTALL_LIBS]/qt5/qml
    }
    else {
        PD_INSTALL_QML = $$[QT_INSTALL_QML]
    }
}

!isEmpty(PD_LIB_PREFIX) {
    INCLUDEPATH += $${PD_LIB_PREFIX}/include
    LIBS += -L$${PD_LIB_PREFIX}/lib
}

# Compilation flags and directories ------------------------------------------

DEPENDPATH += .
MOC_DIR = .moc
OBJECTS_DIR = .obj

!android {
    CONFIG += c++14
}

INCLUDEPATH += $${PWD}/$${TARGET_MAJOR}

LIBS += -lpdcom5 -lQtPdWidgets2 -lexpat

win32 {
    CONFIG += dll
}

# Install libraries and DLLs -------------------------------------------------

libraries.path = $$PD_INSTALL_LIBS

unix | android { # android just for clarification (see comment above)
    libraries.files = $${OUT_PWD}/lib$${TARGET_MAJOR}.so

    INSTALLS += libraries
}


win32 {
    CONFIG(release, debug|release):DIR = "release"
    CONFIG(debug, debug|release):DIR = "debug"

    libraries.files = "$${OUT_PWD}/$${DIR}/lib$${TARGET_MAJOR}.dll.a"

    dlls.path = $$PD_INSTALL_BINS
    dlls.files = "$${OUT_PWD}/$${DIR}/$${TARGET_MAJOR}.dll"

    INSTALLS += dlls libraries
}

# install headers ------------------------------------------------------------

inst_headers.path = $${PD_INSTALL_PREFIX}/include/$${TARGET_MAJOR}

inst_headers.files = \
    $${TARGET_MAJOR}/PdQmlWidgets2.h \
    $${TARGET_MAJOR}/ScalarVariant.h \
    $${TARGET_MAJOR}/ValueRing.h \
    $${TARGET_MAJOR}/VectorVariant.h

INSTALLS += inst_headers

# CMake Config for find_package(pdqmlwidgets2) --------------------------------

QMAKE_SUBSTITUTES += pdqmlwidgets2-config.cmake.in

cmake_files.path = $$PD_INSTALL_LIBS/cmake/$$lower($${TARGET_MAJOR})
cmake_files.files = pdqmlwidgets2-config.cmake

INSTALLS += cmake_files

# install QML files ----------------------------------------------------------

QML_PATH = $${PD_INSTALL_QML}/de/igh/pd.2

inst_qml.path = $${QML_PATH}
inst_qml.files = \
    qml/FontAwesome.otf \
    qml/PdBar.qml \
    qml/PdButton.qml \
    qml/PdCheckBox.qml \
    qml/PdComboBox.qml \
    qml/PdDigital.qml \
    qml/PdImage.qml \
    qml/PdLabel.qml \
    qml/PdLed.qml \
    qml/PdMultiLed.qml \
    qml/PdPushButton.qml \
    qml/PdSlider.qml \
    qml/PdStatusIndicator.qml \
    qml/PdSwitch.qml \
    qml/PdText.qml \
    qml/PdTimeLabel.qml \
    qml/PdToolButton.qml \
    qml/PdTouchEdit.qml \
    qml/PdTumbler.qml \
    qml/PdVectorDigital.qml \
    qml/PdVectorIndicator.qml \
    qml/PdVectorLabel.qml \
    qml/PdVectorLed.qml \
    qml/Sprintf.js \
    qml/TouchEdit.qml \
    qml/TouchEditDialog.qml \
    qml/de_igh_pdQmlWidgets.pri \
    qml/de_igh_pdQmlWidgets.qrc \
    qml/qmldir

inst_qml_style.path = $${QML_PATH}/style
inst_qml_style.files = \
    qml/style/qmldir \
    qml/style/Solarized.qml

inst_qml_quick1.path = $${QML_PATH}/quick1
inst_qml_quick1.files = \
    qml/quick1/qmldir \
    qml/quick1/PdLed.qml \
    qml/quick1/PdMultiLed.qml

inst_qml_material.path = $${QML_PATH}/+material
inst_qml_material.files = \
    qml/+material/qmldir \
    qml/+material/PdBar.qml \
    qml/+material/PdToolButton.qml \
    qml/+material/PdTouchEdit.qml \
    qml/+material/TouchEditDialog.qml \

inst_qml_universal.path = $${QML_PATH}/+universal
inst_qml_universal.files = \
    qml/+universal/qmldir \
    qml/+universal/TouchEditDialog.qml

INSTALLS += \
    inst_qml \
    inst_qml_style \
    inst_qml_quick1 \
    inst_qml_material \
    inst_qml_universal

# define headers and sources -------------------------------------------------

SOURCES += \
    src/ScalarVariant.cpp \
    src/VectorVariant.cpp

# additional files to install ------------------------------------------------

ADDITIONAL_DISTFILES = \
    COPYING \
    COPYING.LESSER \
    README.md

DISTFILES += $${ADDITIONAL_DISTFILES}

#-----------------------------------------------------------------------------
