/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** State: name in pdWidgets: Text.h/cpp
** 
** !!! consider using PdText if the "hash" has gaps !!!
** 
****************************************************************************/


/** Value hash type.
 *
 * This hash shall contain a value object for each possible value to
 * display.
 */

import QtQuick 2.7         
import QtQuick.Controls 2.3

import de.igh.pd 2.0

Label{
    id:control
    
    /**type:var

       Example hash/model with indexRole:
       @code{.qml}
       hash:[
             {key: "None"}
             {key: "a few"}
             {key: "many"} 
             {key: "all"}
            ] 
       @endcode

       Example hash/model without indexRole
       @code{.qml}
       hash:["Car","Person","Animal"]
       @endcode
    */
    property var hash:[]
    property alias model:control.hash

    /**type:var
     * this text is shown if no key is found in hash
     */    
    property string undefText:"undefined"
    
    /**type:var
     * connection to the process variable
     * see @link PdCheckBox @endlink
     */
    property alias variable:scalar
    property alias connection:scalar.connection
    
    /**type:var
     * convinience connection to the process path
     * see @link PdCheckBox @endlink
     */
    property alias path: scalar.path

    /**type:string
     * Text is selected from that role of the model, which key is: IndexRole.
     * When the model has multiple roles, textRole can be set to determine 
     * which role should be displayed. 
     */
    property string indexRole:""

    /** type:bool
     * This signal is emitted when a mouse is hovered over the control. 
     * true: when the mouse moves over the control, false: otherwise
     */
    property alias hovered: ma.containsMouse

    /**type:var
     * Raw Process value
     */
    readonly property alias value:scalar.value

    enabled:scalar.connected            

    PdScalar {
        id:scalar
        onValueChanged:{
	    if(indexRole.length > 0) {
		if(hash[value][indexRole] != undefined) {
                    control.text = hash[value][indexRole];
		} else {
		    control.text = undefText
		}
	    } else { 
		if(hash[value] != undefined) {
                    control.text = hash[value]
		} else {
                    control.text = undefText
		}
            }
	}
    }
    //add a Mousearea to have the hovered event
    MouseArea {
            id: ma
            anchors.fill: parent
            hoverEnabled: true
    }
}
