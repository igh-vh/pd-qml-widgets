/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** State: difference with pdWidgets:
** There is "onValue" and "offValue" defined, here "invert"
**
****************************************************************************/

import QtQuick 2.7         
import QtQuick.Controls 2.3
import de.igh.pd 2.0

/** CheckBox.
 */

CheckBox {
    id: control

    /** variable: var
     *
     * connection to the process variable: to be used like
     *
     * @code{.qml}
     * 
     * variable {
     *     connection: {
     *         "process":pdProcess,
     *         "path":"/control/value",
     *         "scale":1,
     *         "offset":0,
     *         "period":0.1, 
     *         "transmission":Pd.Periodic|Pd.Event|Pd.Poll
     *     }
     * }
     * @endcode
     * 
     * Defaults are:
     * period: 0
     * scale: 1
     * offset: 0
     * transmission: Pd.Event
     *
     */
    property alias variable:scalar

    /**type:var
     * convinience type to reduce typing.
     * Instead of:
     * @code{.qml}
     * variable {
     *    connection: {
     *        "process":pdProcess,
     *        "path":"/control/path"
     *    }
     * }
     * @endcode
     * one can only specify path:"/control/path"
     * 
     * THIS relies on a Pd::Process::defaultProcess which must be set
     * and valid!
     */
    property alias path: scalar.path

    /** type:bool
     * Invert checked state
     */
    property bool invert:false
    
    enabled:scalar.connected
    
    PdScalar {
        id:scalar
        value: (control.checked != invert)
    }
    checked: ((scalar.value != 0) != invert)
}
