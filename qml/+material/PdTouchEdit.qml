import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1

import de.igh.pd 2.0

Label {
    id: control
    property alias connection:scalar.connection
    property alias variable:scalar //!
    property alias path: scalar.path

    property alias decimals:dialog.decimals
    property alias suffix:dialog.suffix
    property alias lowerLimit:dialog.lowerLimit
    property alias upperLimit:dialog.upperLimit
    // deprecated --------------
/*    property alias process: scalar.process
    property alias scale: scalar.scale
    property alias offset: scalar.offset
*/
    // deprecated --------------
    property string oldText: ""
    property alias value:scalar.value
    enabled:scalar.connected
    property alias title:dialog.title
    onDecimalsChanged:{
        scalar.update()
    }
    onSuffixChanged: {
        scalar.update()
    }

    TouchEditDialog {
        id:dialog
        onAccepted: {
            scalar.value = value
        }
    }
    PdScalar {
        id:scalar
        function update() {
            control.text = value.toFixed(control.decimals)+control.suffix
        }
        onValueChanged: {
            update()
        }
    }

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 30
        color:Material.background
        border.color: parent.enabled ? Material.accent : Material.primary
        MouseArea {
            anchors.fill: parent
            onClicked: {         
                dialog.value = scalar.value
                dialog.open() 
            }
        }
    }

    horizontalAlignment: TextInput.AlignRight
    padding:5
}
