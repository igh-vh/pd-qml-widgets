/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** TimeLabel: not checked against QtPdWidgets 
**
** TODO: documentation
**
**
****************************************************************************/

import QtQuick 2.7         
import QtQuick.Controls 2.3

import de.igh.pd 2.0

import "Sprintf.js" as Str

/** Show absolute or relative Time/Date from process value in seconds
    
 */

Label {
    id:control
    property alias variable:scalar
    property alias connection:scalar.connection
    
    property alias path: scalar.path

    property string suffix:""
    
    property var format:Locale.LongFormat 

    property alias value:scalar.value

    enum Mode {  
        Time, 
        DateLocal, 
        DateTimeLocal,
        DateUTC,
        DateTimeUTC
    }

    //change this if mode is fully implemented
    property int mode:PdTimeLabel.Mode.Time

    PdScalar {
        id:scalar
        onValueChanged:{
            switch(control.mode) {
            case PdTimeLabel.Mode.Time:
                var sec = value % 60;
                var min = ((value % 3600)-sec)/60;
                var h = (value - sec - min*60)/3600;
                control.text = Str.sprintf("%02d:%02d:%02d",h,min,sec);
                break;
	    case PdTimeLabel.Mode.DateLocal:
		var date = new Date(value * 1000);
		control.text = date.toLocaleDateString(Qt.locale("de_DE"),format);
		break;
	    case PdTimeLabel.Mode.DateTimeLocal:
		var date = new Date(value * 1000);
		control.text = date.toLocaleString(Qt.locale("de_DE"),format);
		break;
	    case PdTimeLabel.Mode.DateTimeUTC:
		var date = new Date(value * 1000);
		//var dateUTC = new Date(date.getTime() - date.getTimezoneOffset()*60000);
		control.text = date.toUTCString() //dateUTC.toLocaleString(Qt.locale("de_DE"),format);
		break;
            default:
                control.text = "not implemented yet";
                break;
            }
        }
    }
}
