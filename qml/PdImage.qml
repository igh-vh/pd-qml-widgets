/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** State: different to pdWidgets
** has no translation and rotation
**
** Todo: translation and rotation, indexRole
** 
****************************************************************************/

import QtQuick 2.7         
import QtQuick.Controls 2.3

import de.igh.pd 2.0

/** Image display widget.
 *
 * This widget can display images from a hash, depending on the attached
 * process variable's value.
 *
 */

Image{
    id:control
    
    /**type:var
     * List of image url's to be displayed
     @code
     hash:["qrc:/images/on.svg",
           "qrc:/images/off.svg"]
     @endcode
     */
    property var hash:[]
    
    /**type:var
     * connection to the process variable
     * see @link PdCheckBox @endlink
     */
    property alias variable:scalar
    property alias connection:scalar.connection

    /**type:var
     * convinience connection to the process path
     * see @link PdCheckBox @endlink
     */
    property alias path: scalar.path
    
    /**
     * Raw process value
     */
    readonly property alias value:scalar.value
    
    PdScalar {
        id:scalar
        onValueChanged:{
            if(hash[value] != undefined) {
                control.source = hash[value]
            } else {
                control.source = ""
            }
        }
    }
}
